import java.util.Scanner;

public class Player extends Grader{

    public String retrieveName(){
        Scanner nc = new Scanner(System.in);
        // Text art found at https://fsymbols.com/text-art/
        System.out.println("\n" +
                "╭╮╭╮╭╮╱╱╭╮╱╱╱╱╱╱╱╱╱╱╱╱╱╱╭╮╱╱╱╱╭━━━╮╱╱╱╱╱╱╱╱╱╱╱╱╭━━━╮\n" +
                "┃┃┃┃┃┃╱╱┃┃╱╱╱╱╱╱╱╱╱╱╱╱╱╭╯╰╮╱╱╱┃╭━╮┃╱╱╱╱╱╱╱╱╱╱╱╱┃╭━╮┃\n" +
                "┃┃┃┃┃┣━━┫┃╭━━┳━━┳╮╭┳━━╮╰╮╭╋━━╮┃┃╱╰╋╮╭┳━━┳━━┳━━╮┃┃╱╰╋━━┳╮╭┳━━╮\n" +
                "┃╰╯╰╯┃┃━┫┃┃╭━┫╭╮┃╰╯┃┃━┫╱┃┃┃╭╮┃┃┃╭━┫┃┃┃┃━┫━━┫━━┫┃┃╭━┫╭╮┃╰╯┃┃━┫\n" +
                "╰╮╭╮╭┫┃━┫╰┫╰━┫╰╯┃┃┃┃┃━┫╱┃╰┫╰╯┃┃╰┻━┃╰╯┃┃━╋━━┣━━┃┃╰┻━┃╭╮┃┃┃┃┃━┫\n" +
                "╱╰╯╰╯╰━━┻━┻━━┻━━┻┻┻┻━━╯╱╰━┻━━╯╰━━━┻━━┻━━┻━━┻━━╯╰━━━┻╯╰┻┻┻┻━━╯");
        System.out.println("Hello! What is your name?");
        String name = nc.nextLine();
        System.out.println("Well "+ name + " I am thinking of a number between 1 and 20, you have 6 chances.");
        return name;
    }

    static Scanner sc = new Scanner(System.in);

    public static int makeGuess() {
        System.out.println("Take a guess.");
        System.out.println();
        int userGuess = Integer.valueOf(sc.nextLine());
        System.out.println();
        return userGuess;
    }
}
